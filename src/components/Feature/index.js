import React from 'react';
import { FeatureContainer, FeatureButton } from './FeatureElements';

const Feature = () => {
  return (
    <FeatureContainer>
     <a href="index.js"> <h1>Pizza del dia</h1></a>
      <p>Pizza del día
Salsa alfredo de trufa cubierta con polvo de oro de 24 quilates.</p>
      <FeatureButton>Ordenar ahora</FeatureButton>
    
    </FeatureContainer>

  );
};

export default Feature;
