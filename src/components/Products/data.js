import product1 from '../../images/product-1.jpg';
import product2 from '../../images/product-2.jpg';
import product3 from '../../images/product-3.jpg';
import sweet1 from '../../images/sweet3.jpg';
import sweet2 from '../../images/sweet-2.jpg';
import sweet3 from '../../images/sweet-3.jpg';

export const productData = [
  {
    img: product1,
    alt: 'Pizza',
    name: 'Pizza Super Queso',
    desc:
      'Cubierta de queso y peperoni',
    price: '$14.000',
    button: 'Añadir al carrito'
  },
  {
    img: product2,
    alt: 'Pizza',
    name: 'Hawaiana Paraiso',
    desc:
      ' piña, queso y jamon',
    price: '$10.000',
    button: 'Añadir al carrito'
  },
  {
    img: product3,
    alt: 'Pizza',
    name: 'Vegetariana mix',
    desc:
      ' Con los vegetales que tu escojas',
    price: '$10.000',
    button: 'Añadir al carrito'
  }
];

export const productDataTwo = [
  {
    img: sweet2,
    alt: 'Donas',
    name: 'Donchoca',
    desc:
      'Cubierta de chololate',
    price: '$3.000',
    button: 'Añadir al carrito'
  },
  {
    img: sweet3,
    alt: 'Helado',
    name: 'Caramel Wonder',
    desc:
      'Helado de vainilla y chich de chocolate',
    price: '$4.000',
    button: 'Añadir al carrito'
  },
  {
    img: sweet1,
    alt: 'Brownie',
    name: 'Brownie arequipe',
    desc:
      'relleno de arequipe y coco',
    price: '$5.000',
    button: 'Añadir al carrito'
  }
];
